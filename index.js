const Koa = require('koa');
//envia post o get lo parsea y hace algo  
const bodyParser = require('koa-bodyparser');
//permite las peticiones HTTP
const cors = require('kcors');
//encriptacion y seguridad
const jwt = require('koa-jwt');
//manejamiento de rutas
const { controllerRoutes } = require('koa2-controller')
const { rbac, authentication, handleJWTErrors } = require('./app/middelware.js');
const privateKey = require('fs').readFileSync('./cert/jwtRS256.key.pub');

// initialize app
const app = new Koa();

// initialize socketio (mandamos instancia al socket del servidor)
var server = require('http').createServer(app.callback())
var io = require('socket.io')(server)
const SocketIOManager = require('./app/SocketIOManager');

require('./app/models/migrations')//run migrations

const IO = new SocketIOManager(io);

//create the namespace for chat
IO.createSockets('chat');

// initialize public  routes
const publicRoutes = new controllerRoutes({
  path: '/../../app/controllers/public/'
}).routes()

//rutas privadas con seguridad
const privateRoutes = new controllerRoutes({
  path: '/../../app/controllers/private/',
  props: { IO },
  prefix: '/private' //con lo q accedemos, p.ej localhost:3000/private
}).routes()

// console.log(privateRoutes.router)
//utilizamos todas las librerias
app
  .use(bodyParser())
  .use(cors())
  .use(publicRoutes)
  .use(handleJWTErrors)
  .use(jwt({ secret: privateKey }))
  .use(authentication)
  .use(rbac)
  .use(privateRoutes)

server.listen(3000, () => {
  console.log("listen")
})



