
// const { checkType, can } = require('./helpers/utils.js');
const User = require('./models/User');
module.exports.authentication = async (ctx, next) => {

  if (ctx.state.user.payload) {
    const usr = { ...ctx.state.user.payload };
    let model = await User.filter({ mail: usr.mail, password: usr.password })
    if (model.length == 1) {
      ctx.user = model[0];
      return await next();
    } else {
      ctx.status = 401;
      return ctx.body = {
        error: 'Unauthorized token or user'
      }
    }
  } else {
    ctx.status = 401;
    return ctx.body = {
      error: 'Unauthorized token'
    }
  }
}

module.exports.handleJWTErrors = async (ctx, next) => {
  return next().catch((err) => {
    if (401 == err.status) {
      ctx.status = 401;
      ctx.body = {
        error: err.message
      };
    } else {
      throw err;
    }
  });
}

module.exports.rbac = async (ctx, next) => {
  // const permissions = ctx.state.user.payload.permission;
  // const method = ctx.request.method;
  
  return await next()
  // let allPermissions = [];
  // if (checkType.call(permissions) == '[object Object]') {
  //   allPermissions.push(permissions.name);
  // } else {
  //   allPermissions = [...permissions.map(p => p.name)];
  // }

  // if (can(method, allPermissions)) {
  //   await next();
  // } else {
  //   ctx.status = 403
  //   ctx.body = {
  //     error: 'You are not allowed to access this resource'
  //   };
  // }
}
