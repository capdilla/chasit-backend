module.exports.validateBodyPost = async (ctx, next, rules) => {

  let flag = true;
  const { body } = ctx.request;
  let response = { message: 'some elements are require', requires: [] };


  rules.forEach(rule => {
    if (!Object.keys(body).includes(rule.name)) {
      flag = false;
      response.requires.push(rule.name)
    }
  });

  if (flag) {
    return true
  } else {
    return response
  }

  // console.log(flag)

}