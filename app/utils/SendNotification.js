const https = require('https');
module.exports = class SendNotification {

  constructor(keys) {
    this.keys = {
      key: 'MjlhZTVhMTEtMGRjYy00ZjM0LTljZTktYTVmOWQ5MjVmNWY1',
      app_id: '72fd08d6-56e9-49b0-8f01-a6b4395c1d62',
      ...keys
    }
  }


  /**
   * 
   * @param {*} data 
   * include_player_ids Array
   * content Object
   * headings
   * 
   */
  send(data) {

    const options = {
      host: "onesignal.com",
      port: 443,
      path: "/api/v1/notifications",
      method: "POST",
      headers: {
        "Content-Type": "application/json; charset=utf-8",
        "Authorization": `Basic ${this.keys.key}`
      }
    };

    data = { ...data, app_id: this.keys.app_id }

    var req = https.request(options, function (res) {
      res.on('data', function (data) {
        console.log("Response:");
        console.log(JSON.parse(data));
      });
    });

    req.on('error', function (e) {
      console.log("ERROR:");
      console.log(e);
    });

    req.write(JSON.stringify(data));
    req.end();
  }



}