const { koa2Controller } = require('koa2-controller')
const Cars = require('../../models/Cars');
const User = require('../../models/User');

class CarController extends koa2Controller {

  constructor() {
    super();

    this.prefix = "/car";
  }

  paramsBehaviour() {
    return {
      postCreate: {
        rules: [
          { name: 'type', type: 'require' },
          { name: 'license', type: 'require' },
          { name: 'model', type: 'require' },
          { name: 'color', type: 'require' },
          { name: 'brand', type: 'require' },
          { name: 'year', type: 'require' },
        ]
      },
    }
  }

  async getIndex(ctx) {
    const data = await Cars;
    console.log(data)
    return ctx.body = data
  }


  async getMycars(ctx) {
    const user_id = ctx.user.id
    const model = await Cars.filter({ user_id })

    return ctx.body = {
      success: true,
      vehicles: model
    }
  }


  async postCreate(ctx) {
    const user_id = ctx.user.id

    const user = await User.get(user_id)
    console.log(ctx.request.body)
    ctx.request.body.year = parseInt(ctx.request.body.year);
    ctx.request.body.license = ctx.request.body.license.toUpperCase()
    
    let model = new Cars({ ...ctx.request.body });

    model.user = user;

    await model.saveAll({ user: true })

    return ctx.body = {
      success: true,
      vehicle: model
    }
  }

  async putEdit(id, ctx) {

    ctx.request.body.license = ctx.request.body.license.toUpperCase()

    let model = await Cars.get(id).update({ ...ctx.request.body });

    return ctx.body = {
      success: true,
      vehicle: model
    }
  }

  async delDelete(id, ctx) {

    try {
      let model = await Cars.get(id).delete()
    } catch (error) {
      return ctx.body = {
        success: false,
        error
      }
    }

    return ctx.body = {
      success: true,
      vehicle: { id }
    }
  }


  async getReports(ctx) {

    const car_id = ctx.request.query.id
    const model = await Cars.get(id)({ car_id })
    Report.getAll({ model: true }).run().then(function (results) {
      res.json(results);
    });

    return ctx.body = {
      message: 'ok'
    }
  }

}

module.exports = CarController