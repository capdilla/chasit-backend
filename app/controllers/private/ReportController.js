const { koa2Controller } = require('koa2-controller')
const Cars = require('../../models/Reports')

class ReportController extends koa2Controller {



  async getIndex(ctx) {
    const data = await Reports;

    console.log(data)
    return ctx.body = data
  }

  async getAll(ctx) {

    const report_id = ctx.request.query.id
    const model = await Reports.filter({ report_id })

    return ctx.body = {
      message: 'ok'
    }
  }


  async postCreate(ctx) {
    

    let modelReport = new Reports({...ctx.request.body});

    await modelReport.save()

    return ctx.body = {
      sucess: true,
      data:model
    }
  }

  async putEdit(id, ctx) {

    console.log(id);

    let model = await Reports.get(id).update({ ...ctx.request.body });

    model = await model.save();

    return ctx.body = {
      sucess: true,
      data:model
    }

  }


}

module.exports = ReportController