const { koa2Controller } = require('koa2-controller')
const Cars = require('../../models/Cars');
const User = require('../../models/User');
const Report = require('../../models/Reports');

var r = require('../../models/r')
module.exports = class CarController extends koa2Controller {

  constructor() {
    super();

    this.prefix = "/ureport";
  }

  paramsBehaviour() {
    return {
      postCreate: {
        rules: [
          { name: 'type', type: 'require' },
          { name: 'license', type: 'require' },
          { name: 'model', type: 'require' },
          { name: 'color', type: 'require' },
          { name: 'brand', type: 'require' },
          { name: 'year', type: 'require' },
        ]
      },
    }
  }

  async getMyreports(ctx) {
    const user_id = ctx.user.id
    const model = await r.table('report')
      .orderBy(r.desc('createAt'))
      .filter({
        vehicle: { user_id }
      }).run()

    return ctx.body = {
      success: true,
      reports: model
    }
  }

  async delDelete(id, ctx) {
    try {
      let model = await Report.get(id).delete()
    } catch (error) {
      return ctx.body = {
        success: false,
        error
      }
    }

    return ctx.body = {
      success: true,
      report: { id }
    }
  }

}

