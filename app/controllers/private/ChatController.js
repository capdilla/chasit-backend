const { koa2Controller } = require('koa2-controller')
const User = require('../../models/User');
const ChatMain = require('../../models/ChatMain');
const Chat = require('../../models/Chat');
const r = require('../../models/r');
const SendNotifications = require('../../utils/SendNotification')

/**
 * handle all the chat 
 * NOTE: be careful creating multiples nodes with socketio
 */
class ChatControler extends koa2Controller {

  constructor(props) {
    super(props);
    this.prefix = '/chat'
  }

  paramsBehaviour() {
    return {
      postCreate: {
        rules: [
          { name: 'notification_push_user_id', type: 'require' },
          { name: 'text', type: 'require' }
        ]
      },
    }
  }

  async getIndex(ctx) {

    return ctx.body = { message: ctx.user }
  }

  async getMychats(ctx) {

    const { user } = ctx;
    const model = await r.table('chat_main')
      .filter(
        r.row('user1')('mail')
          .eq(user.mail)
          .or(
            r.row('user2')('mail')
              .eq(user.mail)
          )
      )
      .orderBy(r.desc('createdAt'))


    return ctx.body = {
      success: true,
      chats: model
    }

  }

  async getAll(id, ctx) {

    // const chat_main_id = ctx.request.query.chat_id
    const chat_main_id = id
    const model = await Chat.filter({ chat_main_id })
      .orderBy(r.desc('createdAt'))

    model.forEach(m => {
      m._id = m.id
      m.user._id = m.user.id
    });

    // this.props.IO.createSockets('chat');
    this.listenNewMessage({ id: chat_main_id });

    return ctx.body = {
      message: 'ok',
      chats: model,
      chat_main_id
    }
  }


  async postCreate(ctx) {


    let { user } = ctx;
    const { notification_push_user_id, text } = ctx.request.body;
    delete user.password;

    let user2 = await User.filter({ notification_push_user_id });

    if (user2.length > 0) {
      user2 = user2[0];
    } else {
      user2 = {
        id: '',
        name: 'anonimo',
        notification_push_user_id,
      }
    }

    let model = new ChatMain({
      user1: user,
      user2,
      createdAt: new Date(),
    })

    model = await model.save()

    let modelChat = new Chat({
      chat_main_id: model.id,
      text,
      user,
      createdAt: new Date()
    });

    await modelChat.save()

    const data = {
      headings: { "en": `${user.name} quiere contactar contigo.` },
      contents: { "en": text },
      include_player_ids: [notification_push_user_id],
      data: { type: 'chat', data: { chat_main_id: model.id } }
    }

    new SendNotifications().send(data)


    // const model = { id: '88d9a034-94c8-4068-8640-e1b05d15424c' }

    this.listenNewMessage(model);

    return ctx.body = {
      success: true,
      chat_main_id: model.id,
    }
  }

  async postNewMessage(ctx) {

    const { chat_main_id, text } = ctx.request.body;

    const { user } = ctx

    let modelChat = new Chat({
      chat_main_id,
      text,
      user,
      createdAt: new Date()
    })

    await modelChat.save();

    return ctx.body = {
      message: 'ok',
    }
  }


  async getUpdate(ctx) {
    const model = { id: '88d9a034-94c8-4068-8640-e1b05d15424c' }

    let modelChat = await Chat.get('ee349f86-1d1d-46ee-89a5-286cbbc7c8ed')
    modelChat.text = "hola perrito"

    modelChat.save()

    return ctx.body = {
      message: 'ok',
      chatId: model.id
    }
  }

  //read the changes of the model and emit the new change to socketio
  async listenNewMessage(model) {

    //check if the room has actualy a listener
    let listenersMessage = this.props.IO.getIO().of(`/chat`).listenersMessage;
    listenersMessage = (typeof listenersMessage == 'undefined') ? [] : listenersMessage;

    const room = model.id;

    //if exists this listener 
    if (listenersMessage.includes(room)) {
      return
    }

    await Chat.filter({ chat_main_id: model.id })
      .changes().then(newsChats => {
        newsChats.each((err, row) => {
          row._id = row.id
          row.user._id = row.user.id
          this.props.IO.getIO()
            .of(`/chat`)
            .in(room)
            .emit('message', {
              new_data: row
            })
        })
      })

    //if listener room dosen't not exit 
    listenersMessage.push(room)
    this.props.IO.getIO().of(`/chat`).listenersMessage = listenersMessage;
  }


}
module.exports = ChatControler