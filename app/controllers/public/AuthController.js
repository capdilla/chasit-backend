const { koa2Controller } = require('koa2-controller')
const bcrypt = require('bcryptjs');
const User = require('../../models/User');
const Cars = require('../../models/Cars');

const fs = require('fs')
//const privateKey = fs.readFileSync(require('path').join(__dirname, '../../../cert/private.rsa'));
const privateKey = fs.readFileSync(require('path').join(__dirname, '../../../cert/jwtRS256.key'));
const jwt = require('jsonwebtoken');
const { validateBodyPost } = require('../../utils/validate')

class AuthController extends koa2Controller {
  constructor() {
    super();
    this.prefix = "/auth"
  }

  paramsBehaviour() {
    return {
      postLogin: {
        rules: [
          { name: 'mail', type: 'require' },
          { name: 'password', type: 'require' },
        ]
      },

    }
  }

  async postLogin(ctx, next) {

    let response = {
      success: false,
      message: ''
    }
    let { mail, password } = ctx.request.body;

    mail = mail.toLowerCase();

    if (!mail) {
      response.message = "mail is require"

      return ctx.body = response
    }

    let model;
    if (mail) {
      model = await User.filter({ mail });

      if (model.length == 1) {

        if (bcrypt.compareSync(password, model[0].password)) {
          response.message = "welcome";
          response.success = true;

          const claim = {
            payload: model[0]
          }

          const user_id = claim.payload.id
          const vehicles = await Cars.filter({ user_id })

          response = {
            ...response,
            user: claim.payload,
            vehicles,
            token: jwt.sign(claim, privateKey, { algorithm: 'RS256' })
          }

          return ctx.body = response;

        } else {
          response.message = "user or password is incorrect.";
          return ctx.body = response;
        }

      } else if (model.length == 0) {
        response.message = "no mail found.";
        return ctx.body = response;
      }
    }

    return ctx.body = response;
  }

}
module.exports = AuthController