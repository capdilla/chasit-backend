const { koa2Controller } = require('koa2-controller')
const bcrypt = require('bcryptjs');
const User = require('../../models/User');
const Cars = require('../../models/Cars');
const fs = require('fs')
const privateKey = fs.readFileSync(require('path').join(__dirname, '../../../cert/jwtRS256.key'));
const jwt = require('jsonwebtoken');

class UserController extends koa2Controller {

  constructor() {
    super();
    this.prefix = "/users"
  }

  paramsBehaviour() {
    return {
      postCreateUser: {
        rules: [
          { name: 'user', type: 'require' },
          { name: 'cars', type: 'require' },
        ]
      },

    }
  }

  async postCreateUser(ctx) {

    //encrypta el password

    let { password, mail } = ctx.request.body.user;
    if (password) {
      password = this.hashPassword(password);
    }

    mail = mail.toLowerCase()

    let model = new User({
      ...ctx.request.body.user,
      password,
      mail
    });

    console.log(ctx.request.body.user)

    model = await model.save();

    let { license } = ctx.request.body.cars;
    license = license.toUpperCase();

    let car = new Cars({ ...ctx.request.body.cars, license })
    car.user_id = model.id;
    car.user = model;

    await car.save();

    const claim = {
      payload: model
    }

    return ctx.body = {
      success: true,
      user: claim.payload,
      token: jwt.sign(claim, privateKey, { algorithm: 'RS256' }),
      vehicles: [{ ...car }]
      //password: bcrypt.compareSync(password + "hol", passhash)  
    }
  }

  hashPassword(pass) {
    const salt = bcrypt.genSaltSync(10);
    return bcrypt.hashSync(pass, salt);
  }

}

module.exports = UserController