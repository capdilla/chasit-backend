const { koa2Controller } = require('koa2-controller')
const User = require('../../models/User');
const ChatMain = require('../../models/ChatMain');
const Chat = require('../../models/Chat');

/**
 * handle all the chat 
 * NOTE: be careful creating multiples nodes with socketio
 */
class ChatControler extends koa2Controller {

  constructor(props) {
    super(props);
    this.prefix = '/public/chat'
  }

  paramsBehaviour() {
    return {
      postNewMessage: {
        rules: [
          { name: 'chat_main_id', type: 'require' },
          { name: 'text', type: 'require' },
          { name: 'notification_push_user_id', type: 'require' }
        ]
      },
    }
  }


  async getAll(id, ctx) {

    // const chat_main_id = ctx.request.query.chat_id
    const chat_main_id = id
    const model = await Chat.filter({ chat_main_id })
      .orderBy(r.desc('createdAt'))

    model.forEach(m => {
      m._id = m.id
      m.user._id = m.user.id
    });

    // this.props.IO.createSockets('chat');
    this.listenNewMessage({ id: chat_main_id });

    return ctx.body = {
      message: 'ok',
      chats: model,
      chat_main_id
    }
  }

  async postNewMessage(ctx) {

    const { chat_main_id, text, notification_push_user_id } = ctx.request.body;

    const user = {
      id: '',
      name: 'anonimo',
      notification_push_user_id,
    }

    let modelChat = new Chat({
      chat_main_id,
      text,
      user,
      createdAt: new Date()
    })

    await modelChat.save()

    return ctx.body = {
      message: 'ok',
    }
  }

  //read the changes of the model and emit the new change to socketio
  async listenNewMessage(model) {

    //check if the room has actualy a listener
    let listenersMessage = this.props.IO.getIO().of(`/chat`).listenersMessage;
    listenersMessage = (typeof listenersMessage == 'undefined') ? [] : listenersMessage;

    const room = model.id;

    //if exists this listener 
    if (listenersMessage.includes(room)) {
      return
    }

    await Chat.filter({ chat_main_id: model.id })
      .changes().then(newsChats => {
        // console.log(newsChats, "cantidad")
        newsChats.each((err, row) => {
          row._id = row.id
          row.user._id = row.user.id
          this.props.IO.getIO().of(`/chat`).in(room).emit('message', { new_data: row })
        })
      })

    //if listener room dosen't not exit 
    listenersMessage.push(room)
    this.props.IO.getIO().of(`/chat`).listenersMessage = listenersMessage;
  }

}
module.exports = ChatControler