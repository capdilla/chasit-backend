const { koa2Controller } = require('koa2-controller')
const bcrypt = require('bcryptjs');
const Report = require('../../models/Reports');
const Cars = require('../../models/Cars');
const SendNotifications = require('../../utils/SendNotification')


module.exports = class ReportController extends koa2Controller {
  constructor() {
    super();
    this.prefix = "/reports"
  }

  paramsBehaviour() {
    return {
      postCreate: {
        rules: [
          { name: 'reason', type: 'require' },
          { name: 'license', type: 'require' },
        ]
      },

    }
  }

  async postCreate(ctx) {

    let { reason, license, user_notification_id, contact } = ctx.request.body;

    license = license.toUpperCase()

    let vehicle = await Cars.filter({ license }).getJoin({ user: true })

    if (vehicle.length == 0) {
      return ctx.body = {
        success: false,
        message: 'Ups... No se ha encontrado ningun vehiculo 😅'
      }
    }

    vehicle = vehicle[0];

    const { notification_push_user_id } = vehicle.user;
    let obj = { reason, vehicle }
    if (contact) {
      obj.user_notification_id = user_notification_id;
    }

    delete vehicle.user

    let model = new Report(obj);
    console.log(model)
    await model.save();

    const data = {
      headings: { "en": "¡Alguien ha te ha reportado algo! 😨" },
      contents: { "en": reason },
      include_player_ids: [notification_push_user_id],
      data: { type: 'report', data: model }
    }

    new SendNotifications().send(data)

    return ctx.body = {
      success: true,
      message: 'Gracias por tu contribución 😁'
    }
  }


}
// module.exports = AuthController