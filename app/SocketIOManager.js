class SocketIOManager {

  constructor(io) {
    this.io = io;
  }

  createSockets(name) {
    this.io
      .of(`/${name}`)
      .on('connection', (socket) => {

        console.log("connected")
        // once a client has connected, we expect to get a ping from them saying what room they want to join
        socket.on('room', (room) => {
          console.log("listen in room ", room.room)
          socket.join(room.room);
        });

        socket.on('disconnect', () => {
          console.log("disconnect");
          
          socket.disconnect()
        })

      });
  }

  getIO() {
    return this.io;
  }

}

module.exports = SocketIOManager;