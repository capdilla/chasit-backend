const conf = require('../db.json')
const thinky = require('thinky')(conf);
const type = thinky.type;

module.exports = thinky.createModel('chat_main', {
  id: String,
  user1: type.any(),
  user2: type.any(),
  createdAt: type.date()
})
