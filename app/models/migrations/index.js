const User = require('../User');

User.filter({ mail: 'admin@mail.com', name: 'admin' }).then(result => {
  if (result.length == 0) {
    new User({
      mail: 'admin@mail.com',
      name: 'admin',
      password: '$2b$10$5cO06nMlFbenJzW00oymEeHVahMpD9ZfH9.35oZru1dQT9.QgU.Ue'
    }).save().then(res => {
      console.log("user admin created")
    })
  }
})