const conf = require('../db.json')
const thinky = require('thinky')(conf);
const type = thinky.type;

module.exports = thinky.createModel('users', {
  id: String,
  name: String,
  mail: String,
  password: String,
  notification_push_user_id: type.string().default(''),
});

