const conf = require('../db.json')
const thinky = require('thinky')(conf);
const type = thinky.type

const Cars = require('./Cars');

const Report = thinky.createModel('report', {
  id: type.string(),
  reason: type.string(),
  vehicle: type.any(),
  user_notification_id: type.string(),
  createAt: type.date().default(thinky.r.now())
});


module.exports = Report;