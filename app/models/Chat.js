const conf = require('../db.json')
const thinky = require('thinky')(conf);
const type = thinky.type;

module.exports = thinky.createModel('chat', {
  id: String,
  chat_main_id: String,
  text: String,
  user: type.any(),
  image: type.string().default(''),
  sent: type.boolean().default(false),
  recived: type.boolean().default(false),
  createdAt: type.date()
})
