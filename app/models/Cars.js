const conf = require('../db.json')
const thinky = require('thinky')(conf);
const User = require("./User");
const r = thinky.r;

const Cars = thinky.createModel('cars', {
  id: String,
  user_id: String,
  type: String,
  license: String,
  model: String,
  color: String,
  brand: String,
  year: Number
});

// r.expr(obj[key]).contains(records(key))
Cars.belongsTo(User, "user", "user_id", "id");

module.exports = Cars;
